/* eslint-disable curly */
/* eslint-disable brace-style */
const Discord = require('discord.js');
const client = new Discord.Client();
const ytdl = require('ytdl-core-discord');
const fs = require('fs');
const request = require('request');
const gm = require('gm').subClass({ imageMagick: true });

const prefix = '·';

const zik = ['l_internationale.mp3', 'fillon.mp3'];
const dim = ['53,55', '105,578', '703,415', '686,2'];

const bob = (msg) => msg.toLowerCase().split('').map(x => Math.round(require('crypto').randomBytes(1)[0]/256) ? x.toUpperCase() : x).join('');

client.once('ready', () => {
	console.log('Ready!');
});

client.on('message', async message => {
	if (!message.guild) return;

	if (/(?=.*o)(?=.*l)(?=.*d)[old ]+$/gi.test(message.content)) message.delete();

	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(' ');
	const command = args.shift().toLowerCase();

	switch (command) {
	case 'bob':
		message.delete();
		message.channel.send(new Discord.MessageEmbed()
			.setAuthor(message.author.username)
			.setTitle(bob(args.join(' ')))
			.setImage('https://i.imgur.com/ZkY1nyP.png')
			.setColor('#' + Math.random().toString(16).substr(2, 6)));
		break;

	case 'lol':
		// message.delete();
		message.channel.send({ files: [{ attachment: 'jouer_a_lol.mp4' }] });
		break;

	case 'sardraison': {
		message.delete();
		const msg = args.join(' ').split('"');
		message.channel.send(new Discord.MessageEmbed()
			.setAuthor(message.author.username)
			.setTitle((msg[0] === '' ? '"' + bob(msg.splice(0, 2).join('')) + '"\n' : '') + 'MAIS FILS DE PUTE, T\'AS PAS ENCORE COMPRIS QUE ' + msg.join('"').trim().toUpperCase() + ' ?!')
			.setImage('https://i.imgur.com/DADFPXG.png')
			.setColor('#' + Math.random().toString(16).substr(2, 6)));
		break;
	}

	case 'humournoir':
		// message.delete();
		message.channel.send({ files: ['https://i.imgur.com/TmxdJ6j.png'] });
		break;

	case 'ctsur':
		if (message.member.voice.channel) {
			message.member.voice.channel.join().then(con => con.play('ctsur.mp4').on('finish', () => con.disconnect()));
		}
		message.channel.send({ files: [{ attachment: 'ctsur.gif' }] });
		break;

	case 'jouai':
		if (message.member.voice.channel) {
			const url = args.join('').trim();
			const connection = await message.member.voice.channel.join();
			try{
				connection.play(await ytdl(url), { type: 'opus' });
			}catch{
				message.channel.send('Oh ! un lien youtube valide j\'ai dit. T\'as de la flotte dans le crâne ?');
			}
		}else {
			message.reply('T\'es pas dans le channel sale incel.');
		}
		break;

	case 'pedo':
		if (message.member.voice.channel) {
			message.member.voice.channel.join().then(con => con.play('finkie_popol.flac').on('finish', () => con.disconnect()));
		}
		break;

	case 'stadia':
		{
			const url = args.join('').trim();
			request(url, function(err) {
				if (err) {
					message.channel.send('Erreur de téléchargement');
					return;
				}
			}).pipe(fs.createWriteStream('stadia.png')).on('close', function() {
				const img = gm('stadia.png');
				img.out('-matte')
					.out('-virtual-pixel', 'transparent')
					.out('-distort', 'Perspective')
					.out(`0,0,${dim[0]}
						0,%[height],${dim[1]}
						%[width],%[height],${dim[2]}
						%[width],0,${dim[3]}`)
					.out('-extent', '703x578')
					.write('/tmp/stadia_tmp', function(err) {
						if (err) {
							console.log(err.message);
							message.channel.send('Erreur de conversion (lien en PNG only)');
							return;
						}
						gm('wat.png').out('/tmp/stadia_tmp')
							.out('-compose', 'DstOver').out('-composite')
							.write('stadia_final.png', function(err) {
								if (err) {
									console.log(err.message);
									message.channel.send('Erreur de composition');
									return;
								}
								message.delete();
								message.channel.send(new Discord.MessageAttachment('stadia_final.png'));
							});
					});
			});
		}
		break;

	case 'troloin':
		if (message.member.voice.channel) {
			message.member.voice.channel.join().then(con => con.play('dieudo-troloin.mp3').on('finish', () => con.disconnect()));
		}
		break;

	case 'touseul':
		if (message.member.voice.channel) {
			message.member.voice.channel.join().then(con => con.play('dieudo-touseul.mp3').on('finish', () => con.disconnect()));
		}
		break;

	case 'dab':
		// message.delete();
		message.channel.send('https://i.imgur.com/Tj62ddc.gifv');
		break;

	case 'respect':
		message.delete();
		message.channel.send({ files: [{ attachment: 'respect.mp4' }] });
		break;

	case 'damn':
		
		const out_file = 'damn_decorated.jpg';
		const msg = args.join(' ').split('"');
		if(args.length <=0){
			message.channel.send(message.author.username+' devrait songer à être moins con.');			
			return;
		}
		const find_out = 'Just found out about '+msg+'..\n damn that shit sucks man';

		const img = gm('damn.jpg');

		img
		.out('-fill','#000000c8')
		.drawRectangle(0,1100,675,1200)
		.out('-fill','white')
		.fontSize(34)
		.drawText(0,7,find_out,'South')
		.write(out_file,function(err){
			if (err) {
				console.log(err.message);
				message.channel.send('Oupsiiii, c buggué lol');
				return;
			}
			const attachment = new Discord.MessageAttachment(out_file);
			message.delete();
			message.channel.send(attachment);
		});
		break;
		
	case 'help':
		switch(args.shift()) {
		case 'help':
			message.channel.send('Rajoutons dans la liste des cons : ' + message.author.username + '.');
			break;
		case 'bob':
			message.channel.send('·bob <texte> : ' + bob('écrit en mocking bob'));
			break;
		case 'lol':
			message.channel.send('·lol : On va aller vite fait... jouer à LoL.');
			break;
		case 'sardraison':
			message.channel.send('·sardraison ["' + bob('facultatif') + '"] MAIS FILS DE PUTE T\'AS PAS ENCORE COMPRIS QUE <texte> : Pour expliquer un raisonnement comme Saroche le ferait si bien');
			break;
		case 'humournoir':
			message.channel.send('·humournoir : Si t\'aime pas tu ferme ta gueule j\'en ai rien à foutre.');
			break;
		case 'ctsur':
			message.channel.send('·ctsur : MAIS CTSUUUR ENFET C T SUUUR');
			break;
		case 'jouai':
			message.channel.send('·jouai <url> : Joue de la musique à partir d\'un lien youtube :)) (url youtube valide)');
			break;
		case 'pedo':
			message.channel.send('·pedo : C\'était une adolescente.');
			break;
		case 'stadia':
			message.channel.send('·stadia <url> : A utiliser à chaque fois que Bescond snap (PNG only)');
			break;
		case 'troloin':
			message.channel.send('·troloin : J\'ai vu *J\'accuse*, mais j\'étais pas politisé à l\'époque');
			break;
		case 'touseul':
			message.channel.send('·touseul : Je viens de loin, et vu mon teint, je dois faire les choses bien');
			break;
		case 'dab':
			message.channel.send('·dab : Dab sur vos grands morts');
			break;
		case 'respect':
			message.channel.send('·respect : Bonjour, Elise Lucet, France 2 !');
			break;	
		case 'damn':
			message.channel.send('·damn <texte>: Il n\'est pas trop tard pour se politiser enculer');
			break;	
		default:
			message.channel.send('Commandes utilisables :\n' +
				'·help [commande]\n' +
				'·bob <texte>\n' +
				'·lol\n' +
				'·sardraison [citation] <texte>\n' +
				'·humournoir\n' +
				'·ctsur\n' +
				'·jouai <url>\n' +
				'·pedo\n' +
				'·stadia <url>\n' +
				'·troloin\n' +
				'·touseul\n' +
				'·respect\n' +
				'·damn\n' +
				'·dab');
			break;
		}
		break;

	default:
		message.channel.send('·help [commande]');
		break;
	}
});

client.on('voiceStateUpdate', (_, voice) => {
	if (voice.channel && voice.channel.name === 'Le Goulag' && voice.channel.members.size === 1)
		voice.channel.join().then(con => con.play(zik[Math.floor(Math.random() * zik.length)]).on('finish', () => con.disconnect()));
});

client.login(process.env.BESCOND_PROOF_TOKEN);
